/**
 * Created by diego on 10/1/13.
 */
Template.widget.html = function() {

        var type = this.type + "Widget";
        var template = Template[type](this);

        return template;

};

Template.widget.events({
   "click .close": function(event) {

        console.log("Closing " + this.id);
        gridster.remove_widget($('#' + this.id));

   },
   "click .config": function(event) {

       var widget = this;

       $.fallr('show',{
            content: function() {
                return Template["modal"]({
                    type: "widgetConfiguration",
                    widget: widget,
                    title: widget.type.charAt(0).toUpperCase() + widget.type.slice(1) +" Widget Configuration"
                })
            },
            position: 'center',
            bound: '#' + widget.id,
            useOverlay: true
       });

   },
   "mouseenter .config": function(event) {
        $(event.target).addClass("icon-spin");
   },
   "mouseleave .config": function(event) {
       $(event.target).removeClass("icon-spin");
   }
});

Template.widget.rendered = function() {
    $('#' + this.data.id).find('.config').tooltip();
    $('#' + this.data.id).find('.close').tooltip({
        placement: 'top'
    });
};