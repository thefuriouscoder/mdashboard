/**
 * Created by diego on 10/1/13.
 */
Template.dashboard.widgets = function() {

    return [
        {
            type: "number",
            id: "widget_01",
            title: 'Widget 01',
            position: {
                row: 1,
                col: 1
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_02",
            title: 'Widget 02',
            position: {
                row: 2,
                col: 1
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_03",
            title: 'Widget 03',
            position: {
                row: 3,
                col: 1
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_04",
            title: 'Widget 04',
            position: {
                row: 1,
                col: 2
            },
            size: {
                width: 3,
                height: 2
            }
        },
        {
            type: "graph",
            id: "widget_05",
            title: 'Widget 05',
            position: {
                row: 2,
                col: 2
            },
            size: {
                width: 4,
                height: 4
            }
        },
        {
            type: "number",
            id: "widget_06",
            title: 'Widget 06',
            position: {
                row: 1,
                col: 4
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_07",
            title: 'Widget 07',
            position: {
                row: 2,
                col: 4
            },
            size: {
                width: 2,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_08",
            title: 'Widget 08',
            position: {
                row: 3,
                col: 4
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "graph",
            id: "widget_09",
            title: 'Widget 09',
            position: {
                row: 1,
                col: 5
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_10",
            title: 'Widget 10',
            position: {
                row: 3,
                col: 5
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "number",
            id: "widget_11",
            title: 'Widget 11',
            position: {
                row: 1,
                col: 6
            },
            size: {
                width: 1,
                height: 1
            }
        },
        {
            type: "graph",
            id: "widget_12",
            title: 'Widget 12',
            position: {
                row: 2,
                col: 6
            },
            size: {
                width: 1,
                height: 1
            }
        }
    ]

};

Template.dashboard.rendered = function() {

    gridster = $(".gridster ul").gridster({
        widget_margins: [22, 22],
        widget_base_dimensions: [140, 140]
    }).data("gridster");

    $('.container i').tooltip();

};