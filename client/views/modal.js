/**
 * Created by diego on 10/2/13.
 */
Template.modal.body = function() {

    var type = this.type;
    var template = Template[type](this);

    return template;

};