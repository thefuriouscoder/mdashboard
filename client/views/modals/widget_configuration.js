/**
 * Created by diego on 10/2/13.
 */
Template.widgetConfiguration.body = function() {

    var type = this.widget.type + "WidgetConfigurator";
    var template = Template[type](this);

    return template;

};

Template.widgetConfiguration.events({
    "click span.close": function(event) {
        $.fallr("hide");
    }
})
